package scoreboard;

public class ScoreKeeper {

    private final FormatterScore formatterScore;
    private final Score scoreTeamA;
    private final Score scoreTeamB;

    public ScoreKeeper(FormatterScore formatterScore, Score scoreTeamA, Score scoreTeamB) {
        this.formatterScore = formatterScore;
        this.scoreTeamA = scoreTeamA;
        this.scoreTeamB = scoreTeamB;
    }

    public void scoreTeamA1() {
        scoreTeamA.one();
    }
    public void scoreTeamA2() {
        scoreTeamA.two();
    }
    public void scoreTeamA3() {
        scoreTeamA.three();
    }
    public void scoreTeamB1() {
        scoreTeamB.one();
    }
    public void scoreTeamB2() {
        scoreTeamB.two();
    }
    public void scoreTeamB3() {
        scoreTeamB.three();
    }
    public String getScoreTeamA() {
        return formatterScore.formatResult(scoreTeamA.getResult(), scoreTeamB.getResult());
    }
}
