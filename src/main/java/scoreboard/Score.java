package scoreboard;

public class Score {
  private int score;
  private Result result;

  public Score(int score) {
    this.score = score;
    this.result = new Result();
  }

  public Result getResult() {
    result.score = score;
    return result;
  }

  public void one() {
    score += 1;
  }

  public void two() {
    score += 2;
  }

  public void three() {
    score += 3;
  }
}
