package scoreboard;

import java.util.Objects;

public class Result {
  public int score;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Result result = (Result) o;
    return score == result.score;
  }

  @Override
  public int hashCode() {
    return Objects.hash(score);
  }
}
