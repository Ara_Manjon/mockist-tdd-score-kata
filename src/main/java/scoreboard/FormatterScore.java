package scoreboard;

import java.util.Formatter;

public class FormatterScore {

  public String formatResult(Result resultTeamA, Result resultTeamB) {
    return getFormatter(resultTeamA)+ ":" + getFormatter(resultTeamB);
  }

  private Formatter getFormatter(Result result) {
    Formatter fmt = new Formatter();
    return fmt.format("%03d", result.score);
  }


}
