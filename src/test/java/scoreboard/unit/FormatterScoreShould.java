package scoreboard.unit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import scoreboard.FormatterScore;
import scoreboard.Result;

public class FormatterScoreShould {

  @Test
  public void format_result_correctly() {
    FormatterScore formatterScore = new FormatterScore();
    Result resultTeamA = new Result();
    Result resultTeamB = new Result();
    resultTeamA.score = 6;
    resultTeamB.score = 3;
    String expected = "006:003";

    String formattedResult = formatterScore.formatResult(resultTeamA,resultTeamB);

    Assertions.assertEquals(expected, formattedResult);
  }


  @Test
  public void format_result_correctly_with_numbers_bigger_than_9_() {
    FormatterScore formatterScore = new FormatterScore();
    Result resultTeamA = new Result();
    Result resultTeamB = new Result();
    resultTeamA.score = 100;
    resultTeamB.score = 15;
    String expected = "100:015";

    String formattedResult = formatterScore.formatResult(resultTeamA,resultTeamB);

    Assertions.assertEquals(expected, formattedResult);
  }
}
