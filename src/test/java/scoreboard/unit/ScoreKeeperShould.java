package scoreboard.unit;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import scoreboard.FormatterScore;
import scoreboard.Result;
import scoreboard.Score;
import scoreboard.ScoreKeeper;

import static org.mockito.Mockito.*;

public class ScoreKeeperShould {
  @Mock
  private FormatterScore formatterScore;
  @Mock
  private Score scoreTeamA;
  @Mock
  private Score scoreTeamB;
  @Test
  public void get_score_teams_result() {

    MockitoAnnotations.initMocks(this);
    ScoreKeeper scoreKeeper = new ScoreKeeper(formatterScore, scoreTeamA, scoreTeamB);
    Result result = new Result();
    Result result2 = new Result();
    result.score = 0;
    result2.score = 0;
    when(scoreTeamA.getResult()).thenReturn(result);
    when(scoreTeamB.getResult()).thenReturn(result2);

    scoreKeeper.getScoreTeamA();

    verify(formatterScore).formatResult(scoreTeamA.getResult(), scoreTeamB.getResult());
  }

  @Test
  public void delegate_add_one_to_team_A_() {

    MockitoAnnotations.initMocks(this);
    ScoreKeeper scoreKeeper = new ScoreKeeper(formatterScore, scoreTeamA, scoreTeamB);

    scoreKeeper.scoreTeamA1();

    verify(scoreTeamA).one();
    verifyNoInteractions(scoreTeamB);
  }

  @Test
  public void delegate_add_two_to_team_A_() {

    MockitoAnnotations.initMocks(this);
    ScoreKeeper scoreKeeper = new ScoreKeeper(formatterScore, scoreTeamA, scoreTeamB);

    scoreKeeper.scoreTeamA2();

    verify(scoreTeamA).two();
    verifyNoInteractions(scoreTeamB);
  }

  @Test
  public void delegate_add_three_to_team_A_() {

    MockitoAnnotations.initMocks(this);
    ScoreKeeper scoreKeeper = new ScoreKeeper(formatterScore, scoreTeamA, scoreTeamB);

    scoreKeeper.scoreTeamA3();

    verify(scoreTeamA).three();
    verifyNoInteractions(scoreTeamB);
  }

  @Test
  public void delegate_add_one_to_team_B_() {

    MockitoAnnotations.initMocks(this);
    ScoreKeeper scoreKeeper = new ScoreKeeper(formatterScore, scoreTeamA, scoreTeamB);

    scoreKeeper.scoreTeamB1();

    verify(scoreTeamB).one();
    verifyNoInteractions(scoreTeamA);
  }

  @Test
  public void delegate_add_two_to_team_B_() {

    MockitoAnnotations.initMocks(this);
    ScoreKeeper scoreKeeper = new ScoreKeeper(formatterScore, scoreTeamA, scoreTeamB);

    scoreKeeper.scoreTeamB2();

    verify(scoreTeamB).two();
    verifyNoInteractions(scoreTeamA);
  }

  @Test
  public void delegate_add_three_to_team_B_() {

    MockitoAnnotations.initMocks(this);
    ScoreKeeper scoreKeeper = new ScoreKeeper(formatterScore, scoreTeamA, scoreTeamB);

    scoreKeeper.scoreTeamB3();

    verify(scoreTeamB).three();
    verifyNoInteractions(scoreTeamA);
  }

}
