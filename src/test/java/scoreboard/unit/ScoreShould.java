package scoreboard.unit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import scoreboard.Result;
import scoreboard.Score;

public class ScoreShould {

  @Test
  public void add_one_() {

    Score score = new Score(1);
    Result resultExpected = new Result();
    resultExpected.score = 2;
    score.one();

    Assertions.assertEquals(resultExpected, score.getResult());
  }

  @Test
  public void add_two_() {

    Score score = new Score(0);
    Result resultExpected = new Result();
    resultExpected.score = 2;
    score.two();

    Assertions.assertEquals(resultExpected, score.getResult());
  }

  @Test
  public void add_three_() {

    Score score = new Score(0);
    Result resultExpected = new Result();
    resultExpected.score = 3;
    score.three();

    Assertions.assertEquals(resultExpected, score.getResult());
  }
}
