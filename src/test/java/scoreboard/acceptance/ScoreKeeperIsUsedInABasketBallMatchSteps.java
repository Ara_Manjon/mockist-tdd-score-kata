package scoreboard.acceptance;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import scoreboard.FormatterScore;
import scoreboard.ScoreKeeper;
import scoreboard.Score;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


public class ScoreKeeperIsUsedInABasketBallMatchSteps {

    FormatterScore formatterScore = new FormatterScore();
    ScoreKeeper scoreKeeper;

    @Given("a score")
    public void aScore() {
        scoreKeeper = new ScoreKeeper(formatterScore, new Score(0), new Score(0));
    }

    @When("Team A score one point")
    public void teamAScoreOnePoint() {
        scoreKeeper.scoreTeamA1();
    }

    @And("Team B score one points")
    public void teamBScoreOnePoints() {
        scoreKeeper.scoreTeamB1();
    }

    @And("Team B score three points")
    public void teamBScoreThreePoints() {
        scoreKeeper.scoreTeamB3();
    }

    @And("Team A score three points")
    public void teamAScoreThreePoints() {
        scoreKeeper.scoreTeamA3();
    }

    @And("Team B score two points")
    public void teamBScoreTwoPoints() {
        scoreKeeper.scoreTeamB2();
    }

    @And("Team A score two points")
    public void teamAScoreTwoPoints() {
        scoreKeeper.scoreTeamA2();
    }


    @Then("the shown result is {string}")
    public void theShownResultIs(String score) {
        assertThat(scoreKeeper.getScoreTeamA(), is(score));
    }


}
