package scoreboard.acceptance;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        strict = true,
        plugin = {"json:target/cucumber-report.json", "pretty"},
        features = {"src/test/resources/features"})
public class RunCucumberTest {
}
