Feature: Score keeper is used in a basketball match

  Scenario: Team A and Team B alternatively score points and when score is requested is showed in a 7 characters format
    Given a score
    When Team A score one point
    And Team B score one points
    And Team B score three points
    And Team A score three points
    And Team B score three points
    And Team B score two points
    And Team B score three points
    And Team A score two points
    Then the shown result is "006:012"